import java.io.*;
import java.nio.*;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.*;
import java.text.*;

class HttpServer {
    private static String MIME_TYPES = "mime.types";
    private static String HTTP_CONF = "http.conf";
    private static String rootDir;
    private static HashMap<String, String> mimeMap;
    private static HashMap<String, String> confMap;

    private static final int POOL_SIZE = 10;

    private final ServerSocket ss;
    private final ExecutorService pool;

    public HttpServer() throws IOException {
        this.readToMimeMap();
        this.readHttpConfig();
        int port = Integer.parseInt(HttpServer.confMap.get("port"));
        String address = HttpServer.confMap.get("address");
        rootDir = HttpServer.confMap.get("root_dir");

        this.ss = new ServerSocket();
        this.ss.bind(new InetSocketAddress(address, port));

        this.pool = Executors.newFixedThreadPool(POOL_SIZE);
    }

    public static String getRootDir() {
        return rootDir;
    }

    public static HashMap<String, String> getMimeMap() {
        return mimeMap;
    }

    public void serveForever() throws IOException {
        while (true) {
            Socket conn = this.ss.accept();

            this.pool.execute(new Handle(conn));
        }
    }

    private void readToMimeMap() throws IOException {
        HttpServer.mimeMap = (HashMap<String, String>) this.readFile2Map(MIME_TYPES);
    }

    private void readHttpConfig() throws IOException {
        HttpServer.confMap = (HashMap<String, String>) this.readFile2Map(HTTP_CONF);
    }

    private Map<String, String> readFile2Map(String file_name) throws IOException {
        HashMap<String, String> map = new HashMap<>();

        BufferedReader in = new BufferedReader(new FileReader(file_name));

        String line;
        while ((line = in.readLine()) != null) {
            if (line.length() != 0) {
                String key = line.substring(0,line.indexOf(" "));
                String value = line.substring(line.indexOf(" ")).trim();

                map.put(key, value);
            }
        }
        return map;
    }

    public static void main(String[] args) throws IOException  {
        HttpServer sp = new HttpServer();

        sp.serveForever();
    }
}

class Handle extends Thread {
    private final String RESPONSE_MESSAGE = new StringBuilder()
            .append("HTTP/1.1 200 OK\r\n")
            .append("Connection: close\r\n")
            .append("Content-Length: {0}\r\n")
            .append("Content-Type: {1}\r\n")
            .append("Date: {2}\r\n")
            .append("\r\n")
            .toString();
    private final String RESPONSE_ERROR_MESSAGE = new StringBuilder()
            .append("HTTP/1.1 {0} {1}\r\n")
            .append("Connection: close\r\n")
            .append("Date: {2}\r\n\r\n")
            .append("<!DOCTYPE html>")
            .append("<html><body><head><center><h1>")
            .append("404 Not Found")
            .append("</h1></center></head></body></html>")
            .toString();
    private final String logTempl = "{0} {1}";

    private final Socket conn;

    public Handle(Socket conn) {
        this.conn = conn;
    }

    @Override
    public void run() {
        try {
            handleRequest();
        } catch (IOException ex) {
            Logger.getLogger(Handle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void handleRequest() throws IOException {
        InputStream is = this.conn.getInputStream();
        OutputStream os = this.conn.getOutputStream();
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        String firstLine = in.readLine();
        if ( firstLine == null ) return;

        String[] attrs = firstLine.split(" ");
        if ( attrs.length != 3 ) return;

        if ( !attrs[0].equals("GET") ) {
            byte[] msg = genError(405, "Method Not Allowed");
            String log = MessageFormat.format(logTempl, "405", "Method Not Allowed");

            os.write(msg);
            os.flush();
            System.out.println(log);
        }

        doGet(attrs[1]);
    }

    private void doGet(String tailPath) throws IOException {
        OutputStream os = this.conn.getOutputStream();
        String fullPath = get_full_path(tailPath);
        byte[] msg;
        String log;

        if ( !(new File(fullPath)).exists() ) {
            msg = genError(404, "Not Found");
            log = MessageFormat.format(logTempl, "404", "some-strange-url.notfound");
        } else {
            msg = genResponse(fullPath);
            log = MessageFormat.format(logTempl, "200", tailPath);
        }

        os.write(msg);
        os.flush();
        System.out.println(log);
        this.conn.close();
    }

    private String get_full_path(String tail_path) {
        if ("/".equals(tail_path)) {
            tail_path = "/index.html";
        }

        StringBuilder full_path = new StringBuilder();
        full_path.append(HttpServer.getRootDir());
        full_path.append(tail_path.substring(1));

        return full_path.toString();
    }


    private byte[] genResponse(String fullPath) throws IOException {
        byte[] fileBytes = Files.readAllBytes(Paths.get(fullPath));
        String extension = fullPath.substring(fullPath.indexOf(".")+1);

        String msg_hat = MessageFormat.format(RESPONSE_MESSAGE,
                new Integer(fileBytes.length).toString(),
                guessMimeType(extension),
                getServerTime()
        );

        ByteArrayOutputStream responseStream = new ByteArrayOutputStream();
        responseStream.write(msg_hat.getBytes());
        responseStream.write(fileBytes);

        return responseStream.toByteArray();
    }

    private byte[] genError(int code, String message) throws IOException {
        String msg_hat = MessageFormat.format(RESPONSE_ERROR_MESSAGE,
                code,
                message,
                getServerTime()
        );

        return msg_hat.getBytes();
    }

    private String guessMimeType(String extention) {
        if ( HttpServer.getMimeMap().containsKey(extention)) {
            return HttpServer.getMimeMap().get(extention);
        }
        return "application/octet-stream";
    }

    private String getServerTime() {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US);

        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        return dateFormat.format(calendar.getTime());
    }
}
